package main.java.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;

import main.java.model.Tetromino.Direction;
import main.java.model.Tetromino.RotationSense;
import main.java.model.queue.AutofillableQueue;

/**
 * Implementation of the model interface. It uses the Template pattern in order
 * to control the behavior of the score, level and gravity. This version of the
 * tetris uses the following rules: 
 * - score per line(s) cleared = <40|100|300|1200> * (level + 1) 
 *                                  1 2 3 4 lines cleared
 *   the more lines are cleared at once the more will be the reward;
 * - level = lines cleared / 6 + 1
 * - gravity = starts at 1.5 Hz growing by 1/3 on every level up
 */
public class TetrisImpl implements Tetris, Controllable {

	private final ScoreManager scoreManager;
	private final LevelManager levelManager;
	private final GravityManager gravityManager;
	private final InputManager<Controllable> inputManager;
	private final CollisionManager collisionManager;
	private final List<Input> inputs;
	private final List<GameEvent> events;
	private double accumulator;
	private boolean softDrop;
	private int lines;
	private int currentScore;
	private final List<Square> board;
	private Tetromino current;
	private boolean gameOver;
	private Shape shapeHold;
	private final int height;
	private final int width;
	private final AutofillableQueue<Shape> queue;
	private boolean checkHold;
	private TetrominoGenerator generator;
	private static final double GRAVITY_K = 2.50;
	private static final int LINES_TO_LVL_UP = 6;
	private static final double EXP_GROW = 4.0 / 2.75;
	
	/**
	 * Class constructor.
	 */
	public TetrisImpl(int width, int height) {

		this.generator = new TetrominoGenerator(width);
		this.collisionManager = new CollisionManagerImpl();
		this.scoreManager = new ScoreManager() {

			private final Map<Integer, Integer> points = new ImmutableMap.Builder<Integer, Integer>().put(1, 40)
					.put(2, 100).put(3, 300).put(4, 1200).build();

			@Override
			public int get(int n, int level) {
				return points.get(n) * (level + 1);
			}
		};
		this.levelManager = lines -> lines / LINES_TO_LVL_UP + 1;
		this.gravityManager = level -> GRAVITY_K * Math.pow(EXP_GROW, level - 1);
		this.inputManager = new InputManager<Controllable>() {

			private final Map<Input, Consumer<Controllable>> inputs = new ImmutableMap.Builder<Input, Consumer<Controllable>>()
					.put(Input.MOVE_LEFT, t -> t.move(Direction.LEFT))
					.put(Input.MOVE_RIGHT, t -> t.move(Direction.RIGHT))
					.put(Input.ROTATE_CW, t -> t.rotate(RotationSense.CLOCKWISE))
					.put(Input.ROTATE_CCW, t -> t.rotate(RotationSense.COUNTERCLOCKWISE))
					.put(Input.SOFT_DROP_ON, t -> t.setSoftDrop(true))
					.put(Input.SOFT_DROP_OFF, t -> t.setSoftDrop(false)).put(Input.HOLD, t -> t.hold()).build();

			@Override
			public Consumer<Controllable> resolve(Input i) {
				return inputs.get(i);
			}

		};
		this.queue = new AutofillableQueue<>(new Supplier<Shape>() {

			private final List<Shape> shapes = Arrays.asList(Shape.values());
			private final Random rng = new Random();

			@Override
			public Shape get() {
				Shape s = shapes.get(rng.nextInt(shapes.size()));
				return s;
			}
		}, 1);
		this.events = new ArrayList<>();
		this.inputs = new ArrayList<>();
		this.board = new ArrayList<>();
		this.accumulator = 0;
		this.softDrop = false;
		this.shapeHold = null;
		this.gameOver = false;
    this.checkHold = false;
    this.height = height;
    this.width = width;
		getTetromino();

	}

	/**
	 * Getter for the current score.
	 */
	@Override
	public int getScore() {
		return this.currentScore;
	}

	/**
	 * Getter for the cleared lines.
	 */
	@Override
	public int getLines() {
		return this.lines;
	}

	/**
	 * Getter for the current level.
	 */
	@Override
	public int getCurrentLevel() {
		return levelManager.get(this.lines);
	}

	/**
	 * Getter for the current tetromino.
	 */
	@Override
	public Tetromino getCurrentTetromino() {
		return this.current;
	}

	public void setTetrominoCurrent(final Shape s) {
		this.current = generator.newTetromino(s);
	}

	/**
	 * Getter for the board.
	 */
	@Override
	public List<Square> getBoard() {
		final List<Square> t = new ArrayList<>(board);
		t.addAll(current.getAllSquares());
		return t;
	}

	/**
	 * return all square anchored in the board.
	 */
	@Override
	public List<Square> getAnchoredPieces() {
		return Collections.unmodifiableList(board);
	}

	/**
	 * Getter for the queue.
	 */
	@Override
	public List<Shape> getNext() {
		return this.queue.getQueue();
	}

	/**
	 * Getter for the held tetromino.
	 */
	@Override
	public Optional<Shape> getHold() {
		return Optional.ofNullable(shapeHold);
	}

	 /**
   * Getter for the list of events that happened into the game.
   */
	public List<GameEvent> getEvents() {
		List<GameEvent> events = new ArrayList<>(this.events);
		this.events.clear();
		return events;
	}

	/**
	 * Sends the input to the game.
	 */
	public void sendInput(final List<Input> inputs) {
		this.inputs.addAll(inputs);
	}

	/**
	 * Function that updates the state of the game.
	 * @param time	time elapsed
	 */
	@Override
	public void update(final double time) {
		this.accumulator += time;
		this.resolveInput();
		double p;
		while (accumulator > (p = (getPeriod() * getMultiplier()))) {
			this.accumulator -= p;
			this.move(Direction.DOWN);

		}
	}

	/**
	 * Rotates the current tetromino in the given rotation sense.
	 */
	@Override
	public void rotate(final RotationSense r) {
		final Tetromino temp = current.clone();
		temp.rotate(r);
		if (!(collisionManager.check(board, temp, height) 
		        || isOutOfBounds(temp.getAllSquares()))) {
			events.add(GameEvent.ROTATE);
			this.current.rotate(r);
		}

	}

	/**
	 * Moves the current tetromino in the given direction.
	 */
	public void move(final Direction d) {
		final Tetromino temp = current.clone();
		temp.move(d);
		if (d != Direction.DOWN) {
			events.add(GameEvent.MOVE);	
		}
		if (collisionManager.check(board, temp, height) && (d == Direction.DOWN)) {
			anchor();
		} else if (!(isOutOfBounds(temp.getAllSquares()) 
		                || collisionManager.check(board, temp, height))) {
			this.current.move(d);
		}
	}

	/**
	 * Puts the values of the current tetromino in the board.
	 */
	private void anchor() {
		this.board.addAll(current.getAllSquares());/* Anchor the tetromino */
		lineElimination(checkEliminationLine());
		getTetromino();
	}

	/**
	 * Generates a new tetromino.
	 */
	private void getTetromino() {
		this.current = generator.newTetromino(queue.getNext());
		this.checkHold = false;
		if (collisionManager.check(board, current, height)) {
			this.gameOver = true;
		}
	}

	/**
	 * 
	 * Checks if the coordinates of the current tetromino go out of bounds.
	 */
	private boolean isOutOfBounds(final List<Square> app) {
		return app.stream().filter(y -> y.getCoords().getY() >= height 
		                        || y.getCoords().getX() < 0
		                        || y.getCoords().getX() >= width 
		                        || y.getCoords().getY() < 0).count() > 0;

	}

	/**
	 * Setter for the soft-drop mode.
	 */
	@Override
	public void setSoftDrop(final boolean activated) {
		this.softDrop = activated;
	}

	/**
	 * Adds points to the score according to the score manager.
	 * 
	 * @param n
	 *            amount of lines cleared
	 */
	private void addPoints(final int n) {
		this.currentScore += scoreManager.get(n, getCurrentLevel());
	}

	/**
	 * Getter for the gravity value.
	 */
	private double getPeriod() {
		return 1000 / gravityManager.get(getCurrentLevel()); 
	}

	/**
	 * Getter for the time multiplier. The time multiplier is a modifier for the
	 * period of time needed to update the game. If softDrop value is true then the
	 * time multiplier will be 1/2.
	 */
	private double getMultiplier() {
		return softDrop ? 0.5 : 1;
	}

	/**
	 * Applies to the game all the input given.
	 */
	private void resolveInput() {
		this.inputManager.resolveAll(inputs, this);
		this.inputs.clear();
	}

	/**
	 * Returns if the game is over.
	 */
	@Override
	public boolean isGameOver() {
		return this.gameOver;
	}

	/**
	 * Deletes the lines inside the list.
	 * @param list
	 */
	public void lineElimination(final List<Integer> list) {
		if (!list.isEmpty()) {
			this.board.removeAll(board.stream()
			                     .filter(b -> list.contains(b.getCoords().getY()))
			                     .collect(Collectors.toList()));
			events.add(list.size() == 4 ? GameEvent.TETRIS : GameEvent.LINE_CLEAR);
			shiftLines(list);
			this.lines += list.size();
		}
	}

	/**
	 * Creates a list with the line to delete.
	 * 
	 */
	private List<Integer> checkEliminationLine() {
		final List<Integer> list = new ArrayList<>();
		final Map<Integer, Integer> map = fillingMap();
		/*
		 * if the values is equals to the width it means that the rows have to delete
		 */
		map.entrySet().forEach(e -> {
			if (e.getValue() == width) {
				list.add(e.getKey());
			}
		});
		if (list.size() > 0) {
			addPoints(list.size());
		}
		return list;

	}

	/**
	 * Creates a map with key the number of the row in the board and as values the
   *         numbers of square occupated in the corrispettive row.
	 * 
	 */
	private Map<Integer, Integer> fillingMap() {
		final Map<Integer, Integer> map = new HashMap<>();
		Integer numberOfSquare = 1;

		for (int i = 0; i < board.size(); i++) {
			numberOfSquare = 1;
			if (map.containsKey(board.get(i).getCoords().getY())) {
				numberOfSquare = map.get(board.get(i).getCoords().getY()) + 1;
			}
			map.put(board.get(i).getCoords().getY(), numberOfSquare);
		}
		return map;
	}

	
	private void shiftLines(final List<Integer> lines) {
		lines.stream()
		.sorted((a, b) -> a - b)
		.forEach((l) -> {
			this.board.stream()
			.filter(s -> s.getCoords().getY() < l)
			.forEach(s -> s.setCoords(
			            new Point2D(s.getCoords().getX(), s.getCoords().getY() + 1)));

		});
	}

	/**
	 * Apply the function hold.
	 */
	@Override
	public void hold() {
		if (!checkHold && (this.shapeHold == null)) {
				shapeHold = current.getShape();
				this.getTetromino();
		} else if (!checkHold) {
				swap();
				this.checkHold = true;  
		}
		
	}

	/**
	 * Swaps the current tetromino with the tetromino in the hold.
	 */
	private void swap() {
		final Shape temp;

		temp = this.shapeHold;
		this.shapeHold = current.getShape();
		setTetrominoCurrent(temp);
	}
}
