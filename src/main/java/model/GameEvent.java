package main.java.model;

/**
 * Represent the main events dispatched by the model.
 */
public enum GameEvent {

	MOVE,
	ROTATE,
	LINE_CLEAR,
	TETRIS
}
