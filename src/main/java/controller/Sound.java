package main.java.controller;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public enum Sound {

	THEME("theme.wav"),

	GAMEOVER("gameover.wav"),

	LINE("line.wav"),

	MOVE("move.wav"),

	ROTATE("rotate.wav"),

	TETRIS("tetris.wav");

	private Clip clip;

	private Sound(final String soundFileName) {
		try {
			InputStream in = this.getClass().getClassLoader()
			                  .getResourceAsStream(soundFileName);
			AudioInputStream audioInputStream = AudioSystem
			                        .getAudioInputStream(new BufferedInputStream(in));
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Play the current Sounds.
	 */
	public void playSound() {
		clip.setFramePosition(0);
		clip.start();
	}

	/**
	 * Stops the current Sounds.
	 */
	public void stopSound() {
		clip.stop();
	}

	/**
	 * Loop the current Sounds.
	 */
	public void loopSound() {
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	/**
	 *  pre-load all the sound files.
	 */
	public static void init() {
		values();  //calls the constructor
	}
}
