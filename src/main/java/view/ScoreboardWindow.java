package main.java.view;

import java.awt.Toolkit;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.controller.FileManagement;

/**
 * classes that builds the Scoreboard Windows.
 *
 */
public class ScoreboardWindow implements Displayable {
	private static double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 4;
	private static double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2.0;	
	private BorderPane layout;
	private Stage stage;
	private Scene scene;
	private FileManagement file;
	
	/**
	 * Class contructror.
	 * Builds the scene
	 * @param stage
	 */
	public ScoreboardWindow(Stage stage) {
	
		layout = new BorderPane();

		layout.setPadding(new Insets(0,width / 8,0,width / 8));
		this.stage = stage;
		this.file = new FileManagement();
		Label title = new Label("Top Scores");
		title.getStyleClass().add("sub-title-label");
		TilePane top = new TilePane(title);
		top.setAlignment(Pos.CENTER);
		layout.setTop(top);		
		exitBot();
		playerPoint();
		this.scene = new Scene(layout, width, height);

		scene.getStylesheets().add("style.css");
	}
	
	private void exitBot() {
		TilePane bot = new TilePane();
		bot.setPadding(new Insets(0,0,height / 30,0));
		bot.setAlignment(Pos.CENTER);
		bot.setOrientation(Orientation.HORIZONTAL);
		Button exit = new Button("Exit");
		exit.setOnAction(e -> {
			stage.close();
		});
		bot.getChildren().addAll(exit);
		layout.setBottom(bot);
	}	


	private void playerPoint() {
		VBox nameColumn = new VBox();
		VBox pointColumn = new VBox();
		file.listCurrent().forEach(r -> {
			Label player = new Label(r.getName());
			Label score = new Label(String.valueOf(r.getScore()));
			nameColumn.getChildren().add(player);
			pointColumn.getChildren().add(score);
		});
	
		layout.setLeft(nameColumn);
		layout.setRight(pointColumn);
	}

	public Scene getScene() {
		return scene;
	}
	
	public BorderPane getLayout() {
		return layout;
	}

}
