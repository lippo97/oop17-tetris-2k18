package main.java.view;

import static main.java.view.Dimension.HEIGHT;
import static main.java.view.Dimension.WIDTH;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * class that creates the game over windows.
 */
public class GameOverView implements Displayable {
	private Scene scene;
	private VBox layout;
	
	 /**Class constructor.
	  * builds the view
	  * @param stage
	  */
	public GameOverView(Stage stage) {
		layout = new VBox();
		layout.setAlignment(Pos.TOP_CENTER);
		Label title = new Label("GAME OVER");
		title.getStyleClass().addAll("title","newhighscore");


		Label sorry = new Label("I'm really sorry\nyou seemed good");
		sorry.getStyleClass().add("sub-title-label");
		sorry.setStyle("-fx-font-size: 25");
		sorry.setAlignment(Pos.CENTER);
		
		Button back = new Button("back");
		back.setOnAction(e -> {
			stage.setScene(new MainWindows(stage).getScene());
		});
		
		Button high = new Button("High Score");
		high.setOnAction(e -> {
			Stage scoreBoard = new Stage();
			scoreBoard.setTitle("Score board");
			ScoreboardWindow scoreB = new ScoreboardWindow(scoreBoard);	
			scoreBoard.setScene(scoreB.getScene());
			scoreBoard.show();		
		});
		layout.setSpacing(HEIGHT.getValue() / 20);
		layout.getChildren().addAll(title,sorry,high,back);
		layout.setPadding(new Insets(HEIGHT.getValue() / 25,0, HEIGHT.getValue() / 3,0));
		scene = new Scene(layout, WIDTH.getValue(), HEIGHT.getValue());
		scene.getStylesheets().add("style.css");
	}
	
	public Scene getScene() {
		return scene;
	}

}
