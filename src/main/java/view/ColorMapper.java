package main.java.view;

import javafx.scene.paint.Color;
import main.java.model.SquareColor;

/**
 * Interface for a color mapper.
 */
@FunctionalInterface
public interface ColorMapper {

  /**
   * Adapts the color class used in the model to the JavaFX one.
   */
	Color map(SquareColor c);
}
