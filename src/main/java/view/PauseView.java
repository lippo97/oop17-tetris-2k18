package main.java.view;

import static main.java.view.Dimension.HEIGHT;
import static main.java.view.Dimension.WIDTH;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.controller.GameLoop;

/**
 * class that creates the Pause windows and unpauses the game.
 *
 */
public class PauseView implements Displayable {
	private Scene scene;
	
	/**Class constructor, it builds the pause view.
	 * 
	 * @param stage
	 * @param loop Game loop that needs to be resumed
	 */
	public PauseView(Stage stage, GameLoop loop) {
		VBox layout = new VBox();
		layout.setAlignment(Pos.CENTER);
		layout.setSpacing(HEIGHT.getValue() / 4);
		Button exit = new Button("Exit Game");
		exit.setOnAction(e -> {
			Displayable mainMenu = new MainWindows(stage);		
			stage.setScene(mainMenu.getScene());
		});
		
		Button resume = new Button("Resume Game");
			resume.setOnAction(e -> {
				GameViewImpl s = new GameViewImpl(loop,stage);
				stage.setScene(s.getScene());
				loop.setView(s);
				loop.togglePause();
				
			});
		
		layout.getChildren().addAll(resume,exit);
		layout.getStyleClass().add("-fx-background-color: rgba(0,0,0,1)");
		scene = new Scene(layout,WIDTH.getValue(),HEIGHT.getValue());
		scene.getStylesheets().add("style.css");
	}
	
	public Scene getScene() {
		return scene;
	}

}
