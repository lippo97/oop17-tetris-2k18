package main.java.view;

import javafx.application.Application;
import javafx.stage.Stage;
import main.java.controller.Sound;

/**
 * JavaFX entry point, it launches the main menu.
 */
public class MainApplication extends Application {
	
  /**
   * Initializes the sounds and runs the theme song, then it sets the main menu.
   */
	@Override
	public void start(final Stage stage) throws Exception {


		Sound.init();
		Sound.THEME.loopSound();
			

		Displayable mainWindows = new MainWindows(stage);

		stage.setScene(mainWindows.getScene());
		stage.getScene().getStylesheets().add("style.css");
		stage.setOnCloseRequest((e) -> {
			System.exit(0);

		});
		

		stage.show();
	}
}
